import java.awt.EventQueue;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;

import Grafos.ArbolGeneradorMinimo;
import Grafos.Coordenadas;
import Grafos.Grafo;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class mapa extends JFrame {

	private JPanel contentPane;
	private JMapViewer _mapa;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField txtCantidad;

	private MapPolygonImpl _poligono;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					mapa frame = new mapa();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
//
	/**
	 * Create the frame.
	 */
	public void generadorDePuntos(String instancia) {
		int Punto = 1;
		ArrayList<Coordenadas> listaCoordenadas = new ArrayList<Coordenadas>();

		try {
			FileInputStream fis = new FileInputStream(instancia);
			Scanner scanner = new Scanner(fis);

			while (scanner.hasNext()) // Carga de Coordenadas
			{
				Coordenadas coordenadaActual = new Coordenadas(0, 0);

				String PuntoString = String.valueOf(Punto);

				String coordX = scanner.next();
				String coordY = scanner.next();
				double valorX = Double.parseDouble(coordX);
				double valorY = Double.parseDouble(coordY);

				coordenadaActual.setx(valorX);
				coordenadaActual.sety(valorY);

				listaCoordenadas.add(coordenadaActual);
				_mapa.addMapMarker(new MapMarkerDot(PuntoString, new Coordinate(valorX, valorY)));

				Punto = Punto + 1;

			}
			scanner.close();
		} catch (FileNotFoundException s) {
			s.printStackTrace();
		}

		Grafo grafo = new Grafo(listaCoordenadas.size());
		for (int i = 0; i < listaCoordenadas.size() - 1; i++) {
			for (int j = i; j < listaCoordenadas.size(); j++) {
				if (i != j) {
					grafo.agregarArista(i, j, distanciaEntrePuntos(listaCoordenadas.get(i), listaCoordenadas.get(j)));
				}

			}
		}

	}

	public double distanciaEntrePuntos(Coordenadas puntoA, Coordenadas puntoB) {

		double cateto1 = puntoA.getx() + puntoB.getx();
		double cateto2 = puntoA.gety() + puntoB.gety();
		double distancia = Math.sqrt(Math.pow(cateto1, 2) + Math.pow(cateto2, 2));

		return distancia;
	}

	public mapa() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 628, 420);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel mapaVisible = new JPanel();
		mapaVisible.setBounds(10, 11, 422, 359);
		contentPane.add(mapaVisible);

		_mapa = new JMapViewer();
		mapaVisible.add(_mapa);

		JPanel comandos = new JPanel();
		comandos.setBounds(442, 11, 164, 359);
		contentPane.add(comandos);
		comandos.setLayout(null);

		JButton btnAGM = new JButton("Camino M\u00EDnimo");
		btnAGM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_poligono = new MapPolygonImpl();
				_mapa.addMapPolygon(_poligono);
			}
		});
		btnAGM.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnAGM.setBounds(10, 188, 140, 40);
		comandos.add(btnAGM);

		JLabel lblSeleccioneInstancia = new JLabel("Seleccione Instancia");
		lblSeleccioneInstancia.setBounds(27, 11, 127, 23);
		comandos.add(lblSeleccioneInstancia);

		JRadioButton rdbtnInstancia_1 = new JRadioButton("Instancia 1");
		rdbtnInstancia_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_mapa.setDisplayPosition(new Coordinate(-34.532, -58.71), 14); // se le coloca la coordenada de donde
																				// inicializa el mapa
				String Instancia = "Instancia1.txt";
				generadorDePuntos(Instancia);
			}
		});
		buttonGroup.add(rdbtnInstancia_1);
		rdbtnInstancia_1.setBounds(10, 41, 109, 23);
		comandos.add(rdbtnInstancia_1);

		JRadioButton rdbtnInstancia_2 = new JRadioButton("Instancia 2");
		rdbtnInstancia_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_mapa.setDisplayPosition(new Coordinate(-34.529, -58.68), 12); // se le coloca la coordenada de donde
																				// inicializa el mapa
				String Instancia = "Instancia2.txt";
				generadorDePuntos(Instancia);
			}
		});
		buttonGroup.add(rdbtnInstancia_2);
		rdbtnInstancia_2.setBounds(10, 69, 109, 23);
		comandos.add(rdbtnInstancia_2);

		JRadioButton rdbtnInstancia_3 = new JRadioButton("Instancia 3");
		rdbtnInstancia_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_mapa.setDisplayPosition(new Coordinate(-34.6637, -58.41), 12); // se le coloca la coordenada de donde
																				// inicializa el mapa
				String Instancia = "Instancia3.txt";
				generadorDePuntos(Instancia);
			}
		});
		buttonGroup.add(rdbtnInstancia_3);
		rdbtnInstancia_3.setBounds(10, 95, 109, 23);
		comandos.add(rdbtnInstancia_3);

		JRadioButton rdbtnInstancia_4 = new JRadioButton("Instancia 4");
		rdbtnInstancia_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_mapa.setDisplayPosition(new Coordinate(-34.528, -58.52), 13); // se le coloca la coordenada de donde
																				// inicializa el mapa
				String Instancia = "Instancia4.txt";
				generadorDePuntos(Instancia);
			}
		});
		buttonGroup.add(rdbtnInstancia_4);
		rdbtnInstancia_4.setBounds(10, 121, 109, 23);
		comandos.add(rdbtnInstancia_4);

		JRadioButton rdbtnInstancia_5 = new JRadioButton("Instancia 5");
		rdbtnInstancia_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_mapa.setDisplayPosition(new Coordinate(-34.5222, -58.700), 17); // se le coloca la coordenada de donde
																					// inicializa el mapa
				String Instancia = "Instancia5.txt";
				generadorDePuntos(Instancia);
			}
		});
		buttonGroup.add(rdbtnInstancia_5);
		rdbtnInstancia_5.setBounds(10, 147, 109, 23);
		comandos.add(rdbtnInstancia_5);

		JLabel lblCantidadDeClusters = new JLabel("Cantidad de Clusters");
		lblCantidadDeClusters.setHorizontalAlignment(SwingConstants.CENTER);
		lblCantidadDeClusters.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblCantidadDeClusters.setBounds(10, 234, 140, 27);
		comandos.add(lblCantidadDeClusters);

		txtCantidad = new JTextField();
		txtCantidad.setHorizontalAlignment(SwingConstants.CENTER);
		txtCantidad.setBounds(10, 261, 140, 23);
		comandos.add(txtCantidad);
		txtCantidad.setColumns(10);

		JButton btnNewButton = new JButton("Aceptar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.setBounds(10, 306, 99, 42);
		comandos.add(btnNewButton);

	}
}
