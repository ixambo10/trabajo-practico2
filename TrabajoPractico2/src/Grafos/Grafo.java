package Grafos;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Grafo {

	// Representamos el grafo por su matriz de adyacencia
	private double[][] A;
	Coordenadas Punto = new Coordenadas(0, 0);

	// El conjunto de v�rtices est� fijo
	public Grafo(int vertices) {
		A = new double[vertices][vertices];
	}

	// Operaciones sobre aristas
	public void agregarArista(int x, int y, double distancia) {
		verificarIndices(x, y);
		A[x][y] = A[y][x] = distancia;
	}

	public Set<Integer> ListaVertices() {
		Set<Integer> ret = new HashSet<Integer>();
		for (int i = 1; i < tamano(); i++) {
			ret.add(i);
		}
		return ret;
	}

	public double[][] MatrizAdy() {
		return this.A;
	}

	// Cantidad de vertices
	public int tamano() {
		return A.length;
	}

	public String toString() {

		for (int x = 0; x < A.length; x++) {
			for (int y = 0; y < A[x].length; y++) {
				if (x != y) {
					System.out.println(A[x][y]);
				}
			}
		}
		return "Grafo [A=" + Arrays.toString(A) + "]";
	}

	// Lanza excepciones si los �ndices no son v�lidos
	private void verificarIndices(int i, int j) {
		verificarVertice(i);
		verificarVertice(j);

		if (i == j)
			throw new IllegalArgumentException("No existen aristas entre un vertice y si mismo! vertice = " + i);
	}

	private void verificarVertice(int i) {
		if (i < 0 || i >= tamano())
			throw new IllegalArgumentException("El vertice " + i + " no existe!");
	}

	public void borrarArista(int i, int j) {
		verificarIndices(i, j);
		A[i][j] = A[j][i] = 0;
	}

	public double existeArista(int i, int j) {
		verificarIndices(i, j);
		return A[i][j];
	}

	public Set<Integer> vecinos(int i) {
		verificarVertice(i);

		Set<Integer> ret = new HashSet<Integer>();
		for (int j = 0; j < tamano(); ++j)
			if (i != j && existeArista(i, j) != 0)
				ret.add(j);

		return ret;
	}

	public int grado(int i) {
		return vecinos(i).size();
	}

}
