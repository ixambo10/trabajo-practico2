package Grafos;

import java.util.ArrayList;

public class ArbolGeneradorMinimo {
	
	private ArrayList<Integer> ListaVertices; 
	private Grafo grafo;
	
	public ArbolGeneradorMinimo(Grafo g)
	{
		grafo=g;
		ListaVertices = new ArrayList<Integer>();
		
		for (int i=1; i<grafo.tamano(); i++){
			ListaVertices.add(i);
		}

		System.out.println(ListaVertices.toString());
	}
	
	public double[][] AlgPrim() { // Llega la matriz a la que le vamos a aplicar el algoritmo
		
		boolean[] marcados = new boolean[ListaVertices.size()]; // Creamos un vector booleano, para saber cuales est�n marcados  														
		String vertice = String.valueOf(ListaVertices.get(0)); // Le introducimos un nodo aleatorio, o el primero
		return AlgPrim(grafo.MatrizAdy(), marcados, vertice, new double[grafo.MatrizAdy().length][grafo.MatrizAdy().length]); // Llamamos al m�todo recursivo mand�ndole
																							 
	} // un matriz nueva para que en ella nos devuelva el �rbol final
		
	private double[][] AlgPrim(double[][] b, boolean[] marcados, String vertice, double[][] Final) {
		
		marcados[ListaVertices.get(0)] = true;// marcamos el primer nodo
		double aux = -1;
		if (!TodosMarcados(marcados)) { // Mientras que no todos est�n marcados
			for (int i = 0; i < marcados.length; i++) { // Recorremos s�lo las filas de los nodos marcados
				if (marcados[i]) {
					for (int j = 0; j < b.length-1; j++) {
						if (b[i][j] != 0) { // Si la arista existe
							if (!marcados[j]) { // Si el nodo no ha sido marcado antes
								if (aux == -1) { // Esto s�lo se hace una vez
									aux = b[i][j];
								} else {
									aux = Math.min(aux, b[i][j]); // Encontramos la arista m�nima
								}
							}
						}
					}
				}
			}
			// Aqu� buscamos el nodo correspondiente a esa arista m�nima (aux)
			
			
			for (int i = 0; i < marcados.length; i++) {
				if (marcados[i]) {
					for (int j = 0; j < b.length-1; j++) {
						if (b[i][j] == aux) {
							if (!marcados[j]) { // Si no ha sido marcado antes
								Final[i][j] = aux; // Se llena la matriz final con el valor
								Final[j][i] = aux;// Se llena la matriz final con el valor
								System.out.println(String.valueOf(ListaVertices.get(j)));
								return AlgPrim(b, marcados, String.valueOf(ListaVertices.get(j)), Final); // se llama de nuevo al
																								// m�todo con
																								// el nodo a marcar
							}
						}
					}
				}
			}
		}
		return Final;
	}

	public boolean TodosMarcados(boolean[] vertice) { // M�todo para saber si todos est�n marcados
		for (boolean b : vertice) {
			if (!b) {
				return b;
			}
		}
		return true;
	}

}
