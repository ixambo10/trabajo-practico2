package Grafos;


import org.junit.Test;


	public class GrafoTest 
	{
		@Test (expected=IllegalArgumentException.class)
		public void agregarAristaVerticeIgual()
		{
			Grafo g = new Grafo(5);
			g.agregarArista(0, 0, 0);
		}

		@Test (expected=IllegalArgumentException.class)
		public void agregarAristaFueraDeRangoSuperior()
		{
			Grafo g = new Grafo(5);
			g.agregarArista(5, 0, 0);
		}

		@Test (expected=IllegalArgumentException.class)
		public void agregarAristaFueraDeRangoSuperiorJ()
		{
			Grafo g = new Grafo(5);
			g.agregarArista(0, 5, 0);
		}
		
		@Test (expected=IllegalArgumentException.class)
		public void agregarAristaFueraDeRangoInferior()
		{
			Grafo g = new Grafo(5);
			g.agregarArista(-1, 0, 0);
		}

		@Test (expected=IllegalArgumentException.class)
		public void agregarAristaFueraDeRangoInferiorJ()
		{
			Grafo g = new Grafo(5);
			g.agregarArista(0, -1, 0);
		}


	}


