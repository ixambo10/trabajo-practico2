package Grafos;

public class Coordenadas {

	private double x;
	private double y;

	public Coordenadas(double x, double y) {
		this.x=x;
		this.y=y;
	}


	public void setx(double x) {
		this.x = x;
	}

	public void sety(double y) {
		this.y = y;
	}
	
	@Override
	public String toString() {
		return "x= " + getx() + ", y= " + gety() + "\n";
	}

	public double gety() {
		return this.y;
	}

	public double getx() {
		return this.x;
	}

}